import java.util.Scanner;

/**
 * Created by sethf_000 on 10/10/2015.
 */
public class P5_20 {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        System.out.print("Enter a year: ");
        int year = scan.nextInt();

        if (year < 1582){
            System.out.println("Your entry was not after the Gregorian correction and therefore not a leap year.");
        }
        else {

            boolean leapYear = isLeapYear(year);

            if (leapYear) {
                System.out.println(year + " is a leap year.");
            } else {
                System.out.println(year + " is NOT a leap year.");
            }
        }



    }


    public static boolean isLeapYear(int year){


        if (year%4 == 0){
            if (year%100 == 0 && year%400 != 0){
                    return false;
                }
                else{
                    return true;
                }
            }

        return false;

    }


}
