/**
 * Created by sethf_000 on 10/10/2015.
 */
public class P6_12 {

    public static void main(String[] args) {

        boolean inRun = false;
        int[] values = new int[20];

        for (int i = 0; i < 20; i++) {
            values[i] = (int)(Math.random() * 6 + 1);
        }

        int previous = values[0];
        for (int i = 0; i < 19; i++) {
            if (inRun) {
                if (values[i] != previous) {
                    System.out.print(") ");
                    inRun = false;
                }
            }
            if (!inRun) {
                if (values[i] == values[i + 1]) {
                    System.out.print(" (");
                    inRun = true;
                } else {
                    System.out.print(" ");
                }
            }
            previous = values[i];
            System.out.print(values[i]);
        }

        if (inRun && values[values.length - 1] == previous) {
            System.out.print(" " + values[values.length - 1] + ")");
        } else if (inRun && values[values.length - 1] != previous) {
            System.out.print(") " + values[values.length - 1]);
        } else {
            System.out.print(" " + values[values.length - 1]);
        }
    }


}
