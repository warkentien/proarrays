import java.util.Scanner;

/**
 * Created by sethf_000 on 10/10/2015.
 */
public class P5_25 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter a zip code: ");
        String zipCode = scan.next();
        int x = 0;
        printDigit(Integer.parseInt(zipCode));

        System.out.print("|");

        for (int i = 0; i < 5; i++) {
            x = Character.getNumericValue(zipCode.charAt(i));
            printBarCode(x);
        }


        int checkDig = Integer.parseInt(zipCode);
        int total = 0;
        while (checkDig > 0) {
            total += checkDig % 10;
            checkDig /= 10;
        }
        x = 10 - (total % 10);
        printBarCode(x);

        System.out.print("|");


    }

    public static void printDigit(int d){
        System.out.println("Zip code: " + d);
    }

    public static void printBarCode(int zipCode) {

        if (zipCode == 1) {
            System.out.print(":::||");
        }
        if (zipCode == 2) {
            System.out.print("::|:|");
        }
        if (zipCode == 3) {
            System.out.print("::||:");
        }
        if (zipCode == 4) {
            System.out.print(":|::|");
        }
        if (zipCode == 5) {
            System.out.print(":|:|:");
        }
        if (zipCode == 6) {
            System.out.print(":||::");
        }
        if (zipCode == 7) {
            System.out.print("|:::|");
        }
        if (zipCode == 8) {
            System.out.print("|::|:");
        }
        if (zipCode == 9) {
            System.out.print("|:|::");
        }
        if (zipCode == 0) {
            System.out.print("||:::");
        }
    }



}
