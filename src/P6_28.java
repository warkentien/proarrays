import java.util.ArrayList;

/**
 * Created by sethf_000 on 10/11/2015.
 */
public class P6_28 {

    public static void main(String[] args) {

        ArrayList<Integer> a = new ArrayList<Integer>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);

        ArrayList<Integer> b = new ArrayList<Integer>();
        b.add(4);
        b.add(7);
        b.add(9);
        b.add(9);
        b.add(11);

        ArrayList<Integer> sorted = new ArrayList<Integer>();

        sorted = mergeSorted(a, b);

        System.out.println(sorted);

    }

    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> c = new ArrayList<Integer>();

        int[] first = new int[a.size() + 1];
        int[] second = new int[b.size() + 1];
        int totalSize = a.size() + b.size();

        for (int i = 0; i < a.size(); i++) {
            first[i] = a.get(i);
        }

        for (int i = 0; i < b.size(); i++) {
            second[i] = b.get(i);
        }
        first[a.size()] = 9999;
        second[b.size()] = 9999;
        int i = 0;
        int j = 0;

        for (int k = 0; k < totalSize; k++) {
            if(first[i] <= second[j]){
                c.add(first[i]);
                i++;
            }
            else{
                c.add(second[j]);
                j++;
            }
        }
        return c;

    }


}
