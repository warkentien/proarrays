import java.io.*;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * Created by sethf_000 on 10/11/2015.
 */
public class P7_17 {

    public static void main(String[] args) throws IOException {


        try {
            Scanner console = new Scanner(System.in);
            System.out.print("What is the input file name (ex.: sales.txt): ");
            String inputFileName = console.next();
            File inputFile = new File(inputFileName);
            Scanner in = new Scanner(inputFile);
            //in.useDelimiter(";");

            //PrintWriter out = null;
            while (in.hasNextLine()) {
                String line = in.nextLine();
                String[] lines = line.split(";");

                String fileName = lines[1] + ".txt";

                File f = new File(fileName);
                PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(f, true)));

                out.printf(lines[0] + "; " + lines[1] + "; " + lines[2] + "; " + lines[3]);
                out.println();
                out.close();
            }

            in.close();

        }
        catch (IOException exception)
        {
            exception.printStackTrace();
        }

        catch (NoSuchElementException exception)
         {
             System.out.println("File contents invalid.");
         }



    }
}
