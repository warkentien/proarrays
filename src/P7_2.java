import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.NoSuchElementException;

/**
 * Created by sethf_000 on 10/11/2015.
 */
public class P7_2 {
    public static void main(String[] args) throws IOException{

        Scanner console = new Scanner(System.in);
        System.out.print("What is the input file name (hint: test.txt): ");
        String inputFileName = console.next();
        System.out.print("What is the output file name: ");
        String outputFileName = console.next();

        File inputFile = new File(inputFileName);
        Scanner in = new Scanner(inputFile);
        PrintWriter out = new PrintWriter(outputFileName);
        int num = 1;

         while (in.hasNextLine())
            {
                String line = in.nextLine();
                out.printf("/* " + num + " */ " + line + "\n");
                num++;
             }

         in.close();
         out.close();

    }


}
