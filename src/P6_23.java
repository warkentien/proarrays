import java.util.Scanner;

/**
 * Created by sethf_000 on 10/11/2015.
 */
public class P6_23 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("You will be prompted to enter a caption and value.");
        System.out.print("How many data sets do you have?: ");
        int number = scan.nextInt();

        String[] names = new String[number];
        int[] values = new int[number];

        scan.nextLine();
        for (int i = 0; i < number; i++) {
            System.out.print("Enter caption number " + (i + 1) + ": ");
            names[i] = scan.nextLine();
            System.out.println();
        }

        for (int i = 0; i < number; i++) {
            System.out.print("Enter value for " + names[i] + ": ");
            values[i] = scan.nextInt();
            System.out.println();
        }

        int largest = values[0];

        for (int i = 0; i < number; i++) {
            if (values[i] > largest){
                largest = values[i];
            };
        }

        for (int i = 0; i < number; i++) {
            if (values[i] != largest){
                int proportion = (values[i] * 40)/largest;
                System.out.print(names[i] + " ");
                for (int j = 0; j < proportion; j++) {
                    System.out.print("*");
                }
                if(proportion < 1){
                    System.out.print("*");
                }
                System.out.println();
            }
            else{
                System.out.println(names[i] + " ****************************************");
            }
        }





    }
}
