import java.util.Scanner;

/**
 * Created by sethf_000 on 10/10/2015.
 */
public class P5_8 {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        System.out.print("Enter a word: ");
        String word = scan.next();

        String scrambledWord = scramble(word);

        System.out.println("The scrambled word is: " + scrambledWord);

    }

    public static String scramble(String word){

        int length = word.length();
        char spot;
        char spot2;
        String scrambled = "";

        if(length == 3){
            return word;
        }

        int number = 0;
        double rand = Math.random();
        int position = (int)((length-1) * rand);


        if (position == 0){
            position++;
        }

        if (position == word.length()-2){
            position--;
        }

        if(position+1 == word.length() ){
            spot = word.charAt(position);
            spot2 = word.charAt(position-1);

            scrambled = word.substring(0, position-1) + spot + spot2
                    + word.charAt(word.length());

        }
        else{
            spot = word.charAt(position);
            spot2 = word.charAt(position+1);
            scrambled = word.substring(0, position) + spot2 + spot
                    + word.substring(position + 2, word.length());
        }

        if (scrambled.equals(word)){
            scrambled = scramble(scrambled);
        }

        return scrambled;


    }

}
